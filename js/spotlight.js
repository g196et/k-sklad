window.onload = function () {

    document.querySelector('#exampleModal').classList.add('active-modal');


    function remMod() {
        document.querySelector('#exampleModal').classList.remove('active-modal');
    }

    document.querySelector('.close').addEventListener('click', remMod);
    document.querySelector('#exampleModal').addEventListener('click', remMod);


}

$.fn.spotlight = function (opts) {
    $(this).load(function () {
        var pos = $(this).offset();
        var vouale;
        var spotlight;
        var spotimage;
        var spotmask;
        var a = $(this).attr('id')
        var a = $(this).attr('class')
        console.log(a);
        b = a + "_blc closed";
        vouale = document.createElement('div');
        vouale.setAttribute("class", b);
        $(vouale).css('position', 'absolute');
        $(vouale).css('top', pos.top);
        $(vouale).css('right', 0);
        $(vouale).css('cursor', 'none');
        $(vouale).css('overflow', 'hidden');
        if ($(window).width() <= '770') {
            $(vouale).width(635);
            $(vouale).height(205);
        } else if (($(window).width() <= '1300') && ($(window).width() >= '770')) {
            $(vouale).width(695);
            $(vouale).height(265);
        } else if (($(window).width() <= '1380') && ($(window).width() >= '1300')) {
            $(vouale).width(874);
            $(vouale).height(315);
        } else {
            $(vouale).width(1138);
            $(vouale).height(435);
        }
        $(vouale).css('background-image', 'url(images/all_close.png)');
        document.body.appendChild(vouale);
        spotlight = document.createElement("div");
        $(spotlight).width(275);
        $(spotlight).height(275);
        $(spotlight).css('border-radius', '50%');
        $(spotlight).css('overflow', 'hidden');
        $(spotlight).css('display', 'none');
        $(spotlight).css('position', 'absolute');
        $(spotlight).css('left', pos.left);
        $(spotlight).css('top', pos.top);
        $(spotlight).css('z-index', 500);
        spotimage = document.createElement('img');
        spotimage.setAttribute("id", "main_img");
        $(spotimage).attr('src', $(this).attr('src'));
        $(spotimage).css('height', $(this).height());
        $(spotimage).css('width', $(this).width());
        spotmask = document.createElement('img');
        $(spotmask).attr('src', 'images/spotlight.png');
        $(spotmask).css('position', 'absolute');
        $(spotmask).css('left', pos.left);
        $(spotmask).css('top', pos.top);
        $(spotmask).css('z-index', 550);
        spotlight.appendChild(spotimage);
        vouale.appendChild(spotlight);
        vouale.appendChild(spotmask);
        $('body').mousemove(function (e) {
            var x = e.pageX - (pos.left) - 75;
            var y = e.pageY - (pos.top) - 75;
            $(spotlight).css('display', '');
            $(spotlight).css('left', x);
            $(spotlight).css('top', y);
            $(spotmask).css('left', x);
            $(spotmask).css('top', y);
            $(spotimage).css('marginLeft', -x);
            $(spotimage).css('marginTop', -y);
        });
    }).each(function () {
        if (this.complete || (navigator.userAgent.toUpperCase().indexOf('MSIE') && parseInt(navigator.userAgent.toUpperCase().indexOf('MSIE')) == 6)) $(this).trigger("load");
    });
    ;
}
$(".list").find("li").click(function () {
    $(".list").find("li").removeClass("active_list_link");
    $(this).addClass("active_list_link");
});
setTimeout(function () {
    $('.closed').css('opacity', '0.6');
}, 800);
$('#li4_mebel').click(function () {
    $('#main_img').attr('src', 'images/dop/li4_mebel.png');
    $('.img_cont').attr('src', 'images/dop/li4_mebel.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#li4_li4').click(function () {
    $('#main_img').attr('src', 'images/dop/li4_li4.png');
    $('.img_cont').attr('src', 'images/dop/li4_li4.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#li4_clothes').click(function () {
    $('#main_img').attr('src', 'images/dop/li4_clothes.png');
    $('.img_cont').attr('src', 'images/dop/li4_clothes.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#li4_det').click(function () {
    $('#main_img').attr('src', 'images/dop/li4_det.png');
    $('.img_cont').attr('src', 'images/dop/li4_det.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#li4_tur').click(function () {
    $('#main_img').attr('src', 'images/dop/li4_tur.png');
    $('.img_cont').attr('src', 'images/dop/li4_tur.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#li4_moto').click(function () {
    $('#main_img').attr('src', 'images/dop/li4_moto.png');
    $('.img_cont').attr('src', 'images/dop/li4_moto.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#choose_com').click(function () {
    $(".list").find("li").removeClass("active_list_link");
    $('#com_first').addClass("active_list_link");
    $('#main_img').attr('src', 'images/dop/com_mebel.png');
    $('.img_cont').attr('src', 'images/dop/com_mebel.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#choose_man').click(function () {
    $(".list").find("li").removeClass("active_list_link");
    $('#li4_first').addClass("active_list_link");
    $('#main_img').attr('src', 'images/dop/li4_mebel.png');
    $('.img_cont').attr('src', 'images/dop/li4_mebel.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#com_mebel').click(function () {
    $('#main_img').attr('src', 'images/dop/com_mebel.png');
    $('.img_cont').attr('src', 'images/dop/com_mebel.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#com_tovar').click(function () {
    $('#main_img').attr('src', 'images/dop/com_tovar.png');
    $('.img_cont').attr('src', 'images/dop/com_tovar.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#com_archive').click(function () {
    $('#main_img').attr('src', 'images/dop/com_archive.png');
    $('.img_cont').attr('src', 'images/dop/com_archive.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});
$('#com_obor').click(function () {
    $('#main_img').attr('src', 'images/dop/com_obor.png');
    $('.img_cont').attr('src', 'images/dop/com_obor.png');
    setTimeout(function () {
        $('.img_fk1_blc:not(:first)').remove();
        $('.closed').css('opacity', '0.6');
    }, 800);
});